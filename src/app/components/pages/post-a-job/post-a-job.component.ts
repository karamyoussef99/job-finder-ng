import {  Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-post-a-job',
  templateUrl: './post-a-job.component.html',
  styleUrls: ['./post-a-job.component.scss']
})
export class PostAJobComponent implements OnInit {
  jobForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.jobForm = this.formBuilder.group({
      jobTitle: ['', Validators.required],
      jobCategory: [''],
      jobType: ['', Validators.required],
      jobDescription: ['', Validators.required]
    
  });
}
  ngOnInit(): void {   
  }

  onSubmit() {
    const formData = this.jobForm.value;
   console.log(formData)
  }


}
